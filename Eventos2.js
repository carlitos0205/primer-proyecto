// Declarar vector de 6 elementos
let vector = ['abecedario', 5, true, false, 'problema', 619];

console.log('a. Imprimir en la consola usando "for"');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

console.log('\nb. Imprimir en la consola usando "forEach":');
vector.forEach((elemento) => {
  console.log(elemento);
});

console.log('\nc. Imprimir en la consola usando "map":');
vector.map((elemento) => {
  console.log(elemento);
});

console.log('\nd. Imprimir en la consola usando "while":');
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}

console.log('\ne. Imprimir en la consola usando"for..of":');
for (let elemento of vector) {
  console.log(elemento);
}
