// Declarar vector de 6 elementos
let vector = ['abecedario', 5, true, false, 'problema', 619];

// a. Imprimir en la consola el vector
console.log(vector);

// b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log('Primer elemento:', vector[0], '- Ultimo elemento:', vector[5]);

// c. Modificar el valor del tercer elemento
vector[2] = ['frio', 'calor'];

// d. Imprimir en la consola la longitud del vector
console.log('Longitud del vector:', vector.length);

// e. Agregar un elemento al vector usando "push"
vector.push('Elemento agregado');

// f. Eliminar elemento del final e imprimirlo usando "pop"
let eliminado = vector.pop();
console.log('Elemento eliminado:', eliminado);

// g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), 0, 'Elemento mitad');

// h. Eliminar el primer elemento usando "shift"
let primero = vector.shift();
console.log('Elemento eliminado:', primero);

// i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primero);
